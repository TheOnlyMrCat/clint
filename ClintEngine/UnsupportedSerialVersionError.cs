using System;

namespace ClintEngine {
	internal class UnsupportedSerialVersionError : Exception {

		private readonly string _filePath;

		public override string Message => $"Unsupported serial version for file at {_filePath}";

		internal UnsupportedSerialVersionError(string filePath) {
			_filePath = filePath;
		}
	}
}