using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
// ReSharper disable PossibleNullReferenceException

namespace ClintEngine {
	public static class IntentXmlParser {

		public static IntentHandler CreateHandlerFromXMLFile(string path) {
			var reader = XmlReader.Create(path);
			var document = new XmlDocument();
			
			document.Load(reader);
			reader.Close();
			
			var serialVersion = ulong.Parse(document.LastChild.FirstChild.InnerText);
			if (serialVersion != IntentHandler.SERIAL_VERSION) {
				throw new UnsupportedSerialVersionError(Path.GetFullPath(path));
			}
			
			return CreateHandler(document.LastChild, Assembly.GetCallingAssembly());
		}

		private static IntentHandler CreateHandler(XmlNode root, Assembly callingAssembly) {
			var createdHandler = new IntentHandler();
			
			for (var intentNode = root.LastChild.LastChild;
				intentNode != null;
				intentNode = intentNode.NextSibling) {
				var type = callingAssembly.GetType(intentNode.FirstChild.InnerText);
				var args = "";

				if (intentNode.FirstChild.NextSibling.Name == "Args") {
					args = intentNode.FirstChild.NextSibling.InnerText;
				}

				var keywords = new List<string>();
				for (var keywordNode = intentNode.LastChild.FirstChild;
					keywordNode != null;
					keywordNode = keywordNode.NextSibling) {
					keywords.Add(keywordNode.InnerText);
				}
				
				Intent intent;

				if (args == "") {
					intent = (Intent) type.GetConstructor(new[] {typeof(string[])}).Invoke(new object[] {keywords.ToArray()});
				} else {
					var types = new[] {typeof(string[]), typeof(string)};
					intent = (Intent) type.GetConstructor(types)
						.Invoke(new object[] {keywords.ToArray(), args});
				}
				
				createdHandler.AddIntent(intent);
			}

			return createdHandler;
		}
	}
}