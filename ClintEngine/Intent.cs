﻿using System;
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global

namespace ClintEngine {
    
    public abstract class Intent {
        
        public string[] Keywords { get; protected set; }

        protected Intent(string[] keywords) {
            for (var i = 0; i < keywords.Length; i++) {
                keywords[i] = keywords[i].ToLower();
            }

            Keywords = keywords;
        }
        
        public abstract IntentResponse Accept(IntentRequest input);
    }

    public class IntentDelegate : Intent {

        private readonly Func<IntentRequest, IntentResponse> _action;

        public IntentDelegate(string[] keywords, Func<IntentRequest, IntentResponse> action) : base(keywords) {
            for (var i = 0; i < keywords.Length; i++) {
                keywords[i] = keywords[i].ToLower();
            }

            Keywords = keywords;
            _action = action;
        }
        
        public override IntentResponse Accept(IntentRequest input) {
            return _action.Invoke(input);
        }
    }

    public class IntentRequest {
        
        public IntentResponse PreviousResponse { get; }
        public string Content { get; }

        public IntentRequest(string content) : this(null, content) {}
        
        public IntentRequest(IntentResponse previousResponse, string content) {
            PreviousResponse = previousResponse;
            Content = content;
        }
    }
    
    public class IntentResponse {

        public enum ResponseType {
            CONTINUE,
            RETURN
        }
        
        public ResponseType Type { get; }
        public string Content { get; }

        public IntentResponse(ResponseType type, string content) {
            Type = type;
            Content = content;
        }
    }
}
