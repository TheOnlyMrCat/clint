﻿using System.Linq;
using System.Collections.Generic;

namespace ClintEngine {
    
    public class IntentHandler {

        public const ulong SERIAL_VERSION = 0;

        private readonly IList<Intent> _intents;

        public IntentHandler() {
            _intents = new List<Intent>();
        }

        public IntentHandler(IList<Intent> intents) {
            _intents = intents;
        }

        public void AddIntent(Intent intent) {
            _intents.Add(intent);
        }

        public void RemoveIntent(Intent intent) {
            _intents.Remove(intent);
        }

        public List<Intent> GetIntentsForInput(string input) {
            var words = input.Split();
            var possibleIntents = new Dictionary<Intent, double>();
            
            foreach (var intent in _intents)
            foreach (var word in words) {
                if (!intent.Keywords.Contains(word.ToLower())) continue;
                
                if (possibleIntents.ContainsKey(intent)) possibleIntents[intent]+= 1.0 / intent.Keywords.Length;
                else possibleIntents.Add(intent, 1.0 / intent.Keywords.Length);
            }

            var output = new List<Intent>();
            var pairs = possibleIntents.ToList();
            pairs.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value) * -1);

            foreach (var pair in pairs) {
                output.Add(pair.Key);
            }
            
            return output;
        }
    }
}
