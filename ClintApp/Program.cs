﻿using System;
using System.Reflection;
using ClintEngine;

namespace ClintApp {
    public static class MainClass {
        
        public static void Main(string[] args) {
            var handler = IntentXmlParser.CreateHandlerFromXMLFile("../../AppIntents.xml");

            var arg = string.Join(" ", args);
            var response = handler.GetIntentsForInput(arg)[0].Accept(new IntentRequest(null, arg));
            Console.WriteLine($"Type: {response.Type}, Message: \"{response.Content}\"");

            /*Application.Init();
            MainWindow win = new MainWindow();
            win.Show();
            Application.Run();*/
        }
        
    }
}
