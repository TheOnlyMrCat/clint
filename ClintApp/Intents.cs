using ClintEngine;

namespace ClintApp {
	public class OutputIntent : Intent {

		private readonly string _outputString;
		
		public OutputIntent(string[] keywords, string output) : base(keywords) {
			_outputString = output;
		}

		public override IntentResponse Accept(IntentRequest input) {
			return new IntentResponse(IntentResponse.ResponseType.RETURN, _outputString);
		}
	}
}